import argparse
import datetime
import pytz

from scraper import ScrapeBot

"""
 Parametros:  consultar -h
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="Verbose", action="store_true")
    parser.add_argument("-a", "--anual", help="scrip anual",
                        action="store_true")
    parser.add_argument("-d", "--diario", help="scrip diario",
                        action="store_true")
    parser.add_argument("-u", "--usuario", help="scrip diario")

    try:
        args = parser.parse_args()

    except:
        parser.print_help()
        exit(0)

    print("kemok-scrapping ver 1.0")

    if args.verbose:
        print("modo mostrar salida activado")

    if args.usuario:
        usuario = args.usuario
    else:
        usuario = 'Kemokbot'

    scrapingbot = ScrapeBot(args, usuario, 'scrapper_tasa_bancoguatemala_ver2')
    scrapingbot.PrepararDB()
    if args.diario:
        inicio = datetime.datetime.now(pytz.timezone('america/guatemala'))
        salida = scrapingbot.scraper1()
        scrapingbot.RegistrarKemokLog(inicio, salida)

    if args.verbose:
        scrapingbot.leer_data()

    if args.anual:
        inicio = datetime.datetime.now(pytz.timezone('america/guatemala'))
        salida = scrapingbot.scraper2()
        scrapingbot.RegistrarKemokLog(inicio, salida)

    #print(scrapingbot.GetJasonLoadAllocated())

    if args.verbose:
        scrapingbot.leer_data()
